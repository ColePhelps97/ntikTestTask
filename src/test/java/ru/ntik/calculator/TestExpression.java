package ru.ntik.calculator;

import org.junit.*;

import static junit.framework.Assert.*;

public class TestExpression {
	private static Expression exp1;
	private static Expression exp2;
	private static Expression exp3;

	@Test
	public void testExp1() {
		exp1 = new Expression(10d, 20d, Operation.SUM);
		assertEquals(30d, exp1.calculate());
	}
	
	@Test
	public void testExp2() {
		exp1 = new Expression(10d, 20d, Operation.SUB);
		assertEquals(-10d, exp1.calculate());
	}

	@Test
	public void testExp3() {
		exp1 = new Expression(5d, 4d, Operation.MUL);
		assertEquals(20d, exp1.calculate());
	}
	
	@Test
	public void testExp4() {
		exp1 = new Expression(40d, 4d, Operation.DIV);
		assertEquals(10d, exp1.calculate());
	}
	
	@Test
	public void testDivByZero() {
		exp1 = new Expression(5d, 0d, Operation.DIV);
		assertEquals(Double.NaN, exp1.calculate());
	}
	
	@Test
	public void testNan1() {
		exp1 = new Expression(4d, Double.NaN, Operation.SUM);
		assertEquals(Double.NaN, exp1.calculate());
	}
	
	@Test
	public void testNan2() {
		exp1 = new Expression(4d, Double.NaN, Operation.SUB);
		assertEquals(Double.NaN, exp1.calculate());
	}

	@Test
	public void testNan3() {
		exp1 = new Expression(4d, Double.NaN, Operation.MUL);
		assertEquals(Double.NaN, exp1.calculate());
	}

	@Test
	public void testNan4() {
		exp1 = new Expression(4d, Double.NaN, Operation.DIV);
		assertEquals(Double.NaN, exp1.calculate());
	}


	@Test
	public void testNan5() {
		exp1 = new Expression(Double.NaN, 5d, Operation.SUM);
		assertEquals(Double.NaN, exp1.calculate());
	}


	@Test
	public void testNan6() {
		exp1 = new Expression(Double.NaN, 5d, Operation.SUB);
		assertEquals(Double.NaN, exp1.calculate());
	}

	@Test
	public void testNan7() {
		exp1 = new Expression(Double.NaN, 5d, Operation.MUL);
		assertEquals(Double.NaN, exp1.calculate());
	}

	@Test
	public void testNan8() {
		exp1 = new Expression(Double.NaN, 5d, Operation.DIV);
		assertEquals(Double.NaN, exp1.calculate());
	}

	@Test
	public void testExpArg1() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(exp1, 6d, Operation.SUM);
		assertEquals(15d, exp2.calculate());
	}

	@Test
	public void testExpArg2() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(exp1, 6d, Operation.SUB);
		assertEquals(3d, exp2.calculate());
	}

	@Test
	public void testExpArg3() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(exp1, 3d, Operation.MUL);
		assertEquals(27d, exp2.calculate());
	}

	@Test
	public void testExpArg4() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(exp1, 3d, Operation.DIV);
		assertEquals(3d, exp2.calculate());
	}

	@Test
	public void testExpArg5() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(3d, exp1, Operation.SUM);
		assertEquals(12d, exp2.calculate());
	}

	@Test
	public void testExpArg6() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(3d, exp1, Operation.SUB);
		assertEquals(-6d, exp2.calculate());
	}

	@Test
	public void testExpArg7() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(3d, exp1, Operation.MUL);
		assertEquals(27d, exp2.calculate());
	}

	@Test
	public void testExpArg8() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(27d, exp1, Operation.DIV);
		assertEquals(3d, exp2.calculate());
	}

	@Test
	public void testExpArg9() {
		exp1 = new Expression(4d, 5d, Operation.SUM);
		exp2 = new Expression(27d, exp1, Operation.DIV);
		assertEquals(3d, exp2.calculate());
	}
}








