package ru.ntik.calculator;

import java.lang.*;

public class Expression {
	private Object arg1, arg2;
	private Operation operation;
	private Double result;

	public Expression(Double arg1, Double arg2, Operation op) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		operation = op;
	}
	
	public Expression(Double arg1, Expression arg2, Operation op) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		operation = op;
	}

	public Expression(Expression arg1, Double arg2, Operation op) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		operation = op;
	}

	public Expression(Expression arg1, Expression arg2, Operation op) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		operation = op;
	}
	
	public void setArg1(Double value) {
		arg1 = value;
	}

	public void setArg1(Expression exp) {
		arg1 = exp;
	}

	public void setArg2(Double value) {
		arg2 = value;
	}

	public void setArg2(Expression exp) {
		arg2 = exp;
	}

	public void setOperation(Operation op) {
		operation = op;
	}

	public Double calculate() {
		if(arg1 instanceof Double && arg2 instanceof Double) {
			switch (operation) {
				case SUM:
							result = (Double)arg1 + (Double)arg2;
							break;
				case SUB:
							result = (Double)arg1 - (Double)arg2;
							break;
				case MUL: 
							result = (Double)arg1 * (Double)arg2;
							break;
				case DIV:
							if((Double)arg2 != 0) 
								result = (Double)arg1 / (Double)arg2;
							else result = Double.NaN;
							break;
				case NOTDEFINED:
							result = Double.NaN;
							break;
			}
		} else {
			if(arg1 instanceof Expression)
				arg1 = ((Expression)arg1).calculate();	
			if(arg2 instanceof Expression)
				arg2 = ((Expression)arg2).calculate();
			this.calculate();
		}
		return result;
	} 
}	
