package ru.ntik.calculator;

import java.util.*;
import org.xml.sax.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.*;
import javax.xml.stream.*;
import java.io.*;

public class Calculator {
	public static void main(String[] args) {
		try {
			if(args.length != 1) {
				System.out.println("Usage: java Calculator [filename]");
				System.exit(1);
			}
			
			SAXCalculatorParser handler = new SAXCalculatorParser();
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			SAXParser parser = factory.newSAXParser();

			parser.setProperty("http://java.sun.com/xml/jaxp/properties/schemaLanguage", 
      			"http://www.w3.org/2001/XMLSchema");
	
			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(new CalculatorErrorHandler());
			reader.parse(new InputSource(args[0]));


			parser.parse(new File(args[0]), handler);
			ArrayList<Double> res = handler.getResult();
			
			OutputStream outputStream = new FileOutputStream(
				new File("calcResult"+args[0]));
			StringWriter sw = new StringWriter();
			XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(sw);

			TransformerFactory trFactory = TransformerFactory.newInstance();

    		Transformer transformer = trFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "4");

			out.writeStartDocument();
			out.writeStartElement("simpleCalculator");
			out.writeStartElement("expressionResults");

			for(Double result : res) {
				out.writeStartElement("expressionResult");
				out.writeStartElement("result");

				out.writeCharacters(result.toString());

				out.writeEndElement();
				out.writeEndElement();
			}
			
			out.writeEndElement();
			out.writeEndElement();
			out.writeEndDocument();
			
			transformer.transform(new StreamSource(new StringReader(sw.toString())), new StreamResult(outputStream));

			out.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
