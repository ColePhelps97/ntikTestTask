package ru.ntik.calculator;

public enum Operation {
	SUM,
	SUB,
	MUL,
	DIV,
	NOTDEFINED
}
