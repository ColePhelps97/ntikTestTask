package ru.ntik.calculator;

import org.xml.sax.*;

public class CalculatorErrorHandler implements ErrorHandler{
	
	@Override
	public void warning(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());
	}

	@Override
	public void error(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());
		System.exit(1);
	}

	@Override
	public void fatalError(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());
	}
}
