package ru.ntik.calculator;

import java.util.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;
import org.xml.sax.*;


public class SAXCalculatorParser extends DefaultHandler { 
	private Stack<Object> stack = new Stack<>();
	private String currentElement;
	
	@Override
	public void startDocument() throws SAXException {
	}

	@Override
	public void endDocument() throws SAXException {
	}

	@Override 
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		currentElement = qName;
		if(qName.equals("operation")) {
			stack.push((String)atts.getValue(0));
		}
	}

	@Override 
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if(qName.equals("arg")) currentElement="";
		if(qName.equals("operation")) {
			Operation op;
			Expression exp;
			Object arg2 = stack.pop();
			Object arg1 = stack.pop();
			String sOperation = (String)stack.pop();

			switch (sOperation) {
				case "SUM":
					op = Operation.SUM;
					break;
				case "SUB":
					op = Operation.SUB;
					break;
				case "MUL":
					op = Operation.MUL;
					break;
				case "DIV":
					op = Operation.DIV;
					break;
				default: 
					op = Operation.NOTDEFINED;
					break;
			}
			
			if(arg1 instanceof Double && arg2 instanceof Double) {
				exp = new Expression((double)arg1, (double)arg2, op);
				stack.push(exp);
			}

			if(arg1 instanceof Expression && arg2 instanceof Double) {
				exp = new Expression((Expression)arg1, (double)arg2, op);
				stack.push(exp);
			}

			if(arg1 instanceof Double && arg2 instanceof Expression) {
				exp = new Expression((double)arg1, (Expression)arg2, op);
				stack.push(exp);
			}

			if(arg1 instanceof Expression && arg2 instanceof Expression) {
				exp = new Expression((Expression)arg1, (Expression)arg2, op);
				stack.push(exp);
			}
		}
	}

	@Override 
	public void characters(char[] ch, int start, int length) throws SAXException {
		if(currentElement.equals("arg"))
			stack.push(new Double(new String(ch, start, length)));
	}	

	public ArrayList<Double> getResult() {
		ArrayList<Double> res = new ArrayList<>();
		for(Object exp : stack) {
			Double a = ((Expression)exp).calculate();
			res.add(a);
		}
		return res;
	}
}
